#include <stdio.h>
#include <stdlib.h>
#include <SDL2/SDL.h>

/* variables */

static SDL_Window *window;

static void
cleanup(void)
{
  puts("Exit...\n");
}

static void
setup(void)
{
  SDL_Init(SDL_INIT_VIDEO);

  window = SDL_CreateWindow("An SDL2 window",	     /* window title */
			    SDL_WINDOWPOS_UNDEFINED, /* initial x position */
			    SDL_WINDOWPOS_UNDEFINED, /* initial y position */
			    640,		     /* width, in pixels */
			    480,		     /* height, in pixels */
			    SDL_WINDOW_OPENGL	     /* flags - see below */
			    );

  if (window == NULL)
    {
      printf("Could not create window: %s\n", SDL_GetError());
      exit(EXIT_FAILURE);
    }
}

static void
run(void)
{
  SDL_Renderer* renderer = NULL;
  renderer =  SDL_CreateRenderer( window, -1, SDL_RENDERER_ACCELERATED);

  // Set render color to red ( background will be rendered in this color )
  SDL_SetRenderDrawColor( renderer, 255, 0, 0, 255 );

  // Clear winow
  SDL_RenderClear( renderer );

  // Creat a rect at pos ( 50, 50 ) that's 50 pixels wide and 50 pixels high.
  SDL_Rect r;
  r.x = 50;
  r.y = 50;
  r.w = 50;
  r.h = 50;

  // Set render color to blue ( rect will be rendered in this color )
  SDL_SetRenderDrawColor( renderer, 0, 0, 255, 255 );

  // Render rect
  SDL_RenderFillRect( renderer, &r );

  // Render the rect to the screen
  SDL_RenderPresent(renderer);
  
  SDL_Delay(3000);
}

static void
shutdown(void)
{
  SDL_DestroyWindow(window);
  SDL_Quit();
}

int
main(argc, argv)
     int argc;			/* argument count */
     char **argv;		/* argument vector */
{
  atexit(cleanup);

  setup();
  run();
  shutdown();
  
  return EXIT_SUCCESS;
}
